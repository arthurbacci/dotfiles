# echo -ne "\033]0; YOUR_WINDOW_TITLE_HERE \007"

starship init fish | source

[ -f "/home/arthur/.ghcup/env" ] && source "/home/arthur/.ghcup/env"

# Remove "Welcome to fish [...]" message
set fish_greeting

source "/home/arthur/.config/fish/keys.fish"

export PATH="$HOME/.local/bin:$PATH"
cd

